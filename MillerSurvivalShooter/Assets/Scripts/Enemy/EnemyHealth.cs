﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
    public float burnSpeed = .2f;
    bool burnPlayed;
    float timer;


    Animator anim;
    AudioSource enemyAudio;
    public ParticleSystem hitParticles;
    public ParticleSystem burnParticles;
    CapsuleCollider capsuleCollider;

    bool isDead;
    bool isSinking;
    public bool isBurning;


    void PlayAnim() {
        burnParticles.Play();
        
        burnPlayed = true;
    }
    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        burnPlayed = false;
        


        currentHealth = startingHealth;
        timer = 0f;
    }


    void Update ()
    {

        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
        if (isBurning) {
            if (!burnPlayed) PlayAnim();
            
            if (currentHealth <= 0)
            {
                Death();
                isBurning = false;
            }
            else if (timer >= burnSpeed)
            {
                timer = 0f;
                currentHealth -= 5;
            }
            else { timer += Time.deltaTime; }
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if(isDead)
            return;

        enemyAudio.Play ();

        currentHealth -= amount;
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
    }


    public void StartSinking ()
    {
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue;
        Destroy (gameObject, 2f);
    }
}
