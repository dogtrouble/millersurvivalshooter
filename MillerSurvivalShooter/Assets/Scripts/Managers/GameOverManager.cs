﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public TeddyManager tedMan;


    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
        tedMan = GameObject.FindWithTag("TedMan").GetComponent<TeddyManager>();
		anim.SetTrigger ("GameBegun");
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
        }
    }
    public void TeddyUpdate() {
        anim.SetTrigger("TeddyAppearance");
        
    }
}
