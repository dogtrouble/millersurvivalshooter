﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    int highScore;

    Text text;
    

    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
        highScore = PlayerPrefs.GetInt("highScore", 0);
    }


    void Update ()
    {
        text.text = "Score: " + score + "\nHigh Score: " + highScore;
        if (score > highScore) {
            highScore = score;
            PlayerPrefs.SetInt("highScore", 0);
        }
    }
}
