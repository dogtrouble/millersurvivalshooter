﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();

    }
    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    void Move(float h, float v) {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning() {
        // Creates a ray that indicates the screen point of the mouse position
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // This variable is output when Raycast is called 
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask)) {
            // If the ray cast succeeds from the camera ray to floorMask within camRay Length
            Vector3 playerToMouse = floorHit.point - transform.position;
            // The above vector points from the floor hit point to the player's location
            playerToMouse.y = 0f;
            // y should not be accounted for, as the game is flat

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            // Rotates player based off a Quaternion derived from above vector
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v) {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);

    }
}
