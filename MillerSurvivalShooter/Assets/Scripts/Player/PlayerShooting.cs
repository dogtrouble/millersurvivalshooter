﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float timeBetweenPickups = .3f;
    public float range = 100f;
    public int numShots = 5;
    public float degreeShots = 1.5f;

    float timer;
    float pickupTime;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;

    public GameObject candle;
    GameObject candleInstance;

    public bool hasCandle;
    public bool canPickup;
    bool pickupTimeout;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
        hasCandle = false;
        canPickup = false;
        pickupTimeout = false;
        candleInstance = GameObject.FindWithTag("Candle");
    }


    void Update ()
    {
        timer += Time.deltaTime;
        pickupTime += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if (pickupTime >= timeBetweenPickups) {
            pickupTimeout = false;
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }

        if (Input.GetButton("Fire2") && hasCandle && !pickupTimeout)
        {
            candleInstance = Instantiate(candle, this.transform.position, this.transform.rotation);
            hasCandle = false;
            pickupTime = 0f;
            pickupTimeout = true;

        }
        else if (Input.GetButton("Fire2") && !hasCandle && canPickup && !pickupTimeout) {
            hasCandle = true;
            canPickup = false;
            Destroy(candleInstance.gameObject);
            pickupTime = 0f;
            pickupTimeout = true;
        }
        if (Input.GetButtonUp("Fire1")) {
            gunLight.enabled = false;
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        //gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        /*gunParticles.Stop ();
        gunParticles.Play ();
        */

        //gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

       


        shootRay.origin = transform.position;
         for (int i = 0; i < numShots; i++) {
            int j = i - numShots/2;
            
            //Vector3 myDirection = Quaternion.Euler(0, 0, j*degreeShots) * transform.forward;
			Vector3 myDirection = transform.forward + .01f * (j*degreeShots*transform.right);
			shootRay.direction = myDirection;
            if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot, shootHit.point);
                }
                gunLine.SetPosition(1, shootHit.point);
            }
            else
            {
                gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
            }
        }
        shootRay.direction = transform.forward;
        /*
        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
        */
    }
}
