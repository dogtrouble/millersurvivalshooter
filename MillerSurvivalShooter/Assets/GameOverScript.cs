﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {
    Text text;
	// Use this for initialization
	void Awake() {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (TeddyCount.teddies == 0)
        {
            text.text = "You Found all the Teddies! Great Job!";
        }
        else {
            text.text = "Game Over!";
        }
	}
}
