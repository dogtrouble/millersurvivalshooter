﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyManager : MonoBehaviour {

	public GameObject teddy;
	public float spawnTime = 20f;
	public Transform[] spawnPoints;
	int spawnPoint;
	public int teddiesLeft;
	public int totalTeddies = 4;
	float clock;
    GameOverManager gMan;

	// Use this for initialization
	void Start () {
		teddiesLeft = totalTeddies;
		spawnPoint = 0;
		clock = 0f;
		spawnPoint = Random.Range (0, teddiesLeft);
        gMan = GameObject.FindWithTag("UI").GetComponent<GameOverManager>();
	}
	
	// Update is called once per frame
	void Update () {
		clock += Time.deltaTime;
		if (clock >= spawnTime && teddiesLeft > 0) {
			Instantiate (teddy, spawnPoints [spawnPoint].position, spawnPoints [spawnPoint].rotation);
			print(spawnPoint + "HAHAH");
			spawnPoint = (spawnPoint + 1) % totalTeddies;
			teddiesLeft--;
			clock = 0f;
            gMan.TeddyUpdate();
            
		}
	}
}
