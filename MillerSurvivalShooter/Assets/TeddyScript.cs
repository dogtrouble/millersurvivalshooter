﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyScript : MonoBehaviour {
	AudioSource teddyAudio;
    // Use this for initialization
	float timer;
	bool waitToDie;
    void Start () {
		teddyAudio = GetComponent<AudioSource> ();
		timer = 0f;
		waitToDie = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > 1f) {
			Destroy (this.gameObject);
		}
		if (waitToDie) {
			timer += Time.deltaTime;
		}
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
            TeddyCount.teddies--;
            ScoreManager.score += 1000;
			teddyAudio.Play ();
			waitToDie = true;
			GetComponent<Renderer>().enabled = false;
		}
	}
}
