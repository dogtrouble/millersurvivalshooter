﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandlePickkup : MonoBehaviour {

    GameObject player;
    GameObject thisEnemy;
    PlayerShooting shoot;
    EnemyHealth health;
    Collider sphere;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
        shoot = player.GetComponent<PlayerShooting>();
        shoot = player.GetComponentInChildren<PlayerShooting>();
        sphere = this.GetComponent<SphereCollider>();
        sphere.enabled = true;
        print("Hello");
        print(shoot.hasCandle);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    

    void OnTriggerEnter(Collider other)
    {
        print(shoot.hasCandle);

        if (other.tag == "Player" && !shoot.hasCandle)
        {
            shoot.canPickup = true;
        }
        else if (other.tag == "Enemy") {
            thisEnemy = other.gameObject;
            health = thisEnemy.GetComponentInChildren<EnemyHealth>();
            health.isBurning = true;
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") {
            shoot.canPickup = false;
        }
    }
}
