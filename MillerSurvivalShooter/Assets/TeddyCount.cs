﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeddyCount : MonoBehaviour {
    public static int teddies;
    public TeddyManager tedMan;
    Text text;
	// Use this for initialization
	void Awake () {
        text = GetComponent<Text>();
        teddies = tedMan.totalTeddies;
	}
	
	// Update is called once per frame
	void Update () {
        if (teddies > 0)
        {
            text.text = "Teddies Left: " + teddies;
        }
        else {
            text.text = "You found all the teddies!";
        }
	}
}
